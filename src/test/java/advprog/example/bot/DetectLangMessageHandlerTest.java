package advprog.example.bot;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import advprog.example.bot.DetectLangMessageHandler;

import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;



public class DetectLangMessageHandlerTest {

    @Test
    void testIsUrlShouldReturnTrueOnUrlString() {
        String string = "https://www.google.com";
        assertTrue(DetectLangMessageHandler.isUrl(string));
    }

    @Test
    void testIsUrlShouldReturnFalseOnUrlString() {
        String string = "Halo, selamat pagi!";
        assertFalse(DetectLangMessageHandler.isUrl(string));
    }

    @Test
    void testGenerateEndpoint() {
        String baseUrl = "www.google.com/";
        Map<String, String> queryStrings = new HashMap<String, String>();
        queryStrings.put("q1", "v1");
        queryStrings.put("q2", "v2");
        assertEquals("www.google.com/?q1=v1&q2=v2",
                DetectLangMessageHandler.generateEndpoint(baseUrl, queryStrings));
    }

}
